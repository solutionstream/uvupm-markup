document.addEventListener('DOMContentLoaded', function() {

  /* Calendar */

  var calendarEl = document.getElementById('calendar');

  if (typeof(calendarEl) != 'undefined' && calendarEl != null) {
    var calendar = new FullCalendar.Calendar(calendarEl, {
      initialView: 'dayGridMonth',
      height: 'auto',
      headerToolbar: {
        start: 'prev',
        center: 'title',
        end: 'next'
      },
      titleFormat: {
        month: 'long'
      },
      fixedWeekCount: false,
      eventSources: [
        {
          events: [
            { // this object will be "parsed" into an Event Object
              title: '', // a property!
              start: '2021-07-01', // a property!
              end: '2021-07-08' // a property! ** see important note below about 'end' **
            },
          ],
          color: '#D9241F'
        },
        {
          events: [
            { // this object will be "parsed" into an Event Object
              title: '', // a property!
              start: '2021-07-04', // a property!
              end: '2021-07-10' // a property! ** see important note below about 'end' **
            },
          ],
          color: '#2F80ED'
        },
        {
          events: [
            { // this object will be "parsed" into an Event Object
              title: '', // a property!
              start: '2021-07-19', // a property!
              end: '2021-07-24' // a property! ** see important note below about 'end' **
            },
          ],
          color: '#219653'
        },

      ]
      
    });
    calendar.render();
  }


  /* WYSIWYG */
  var editor = document.querySelectorAll('.has-editor');
  var toolbarOptions = ['bold', 'italic', 'underline', 'strike', 'code-block', { 'header': [1, 2, 3, false] }, { 'list': 'ordered'}, { 'list': 'bullet' }, 'blockquote', 'link'];

  if (typeof(editor) != 'undefined' && editor != null) {
    for (var i = 0; i < editor.length; i++) {
      var quill = new Quill(editor[i], {
        theme: 'snow',
        modules: {
          toolbar: toolbarOptions
        }
      });
    }
  }



  /* Modal */

  MicroModal.init({
    onShow: function() {
      document.body.classList.add("no-scroll");
    },
    onClose: function() {
      document.body.classList.remove("no-scroll");
    }
  });



  /*  Masonry Layouts */

  var elem = document.querySelector('.masonry');
  var msnry = new Masonry( elem, {
    // options
    itemSelector: '.masonry-item',
    gutter: 30
  });


  var inputs = document.getElementsByClassName("safeToDeleteId");

  for (var v = 0; v < inputs.length; v++) {
    inputs[v].indeterminate = true;
  }

});